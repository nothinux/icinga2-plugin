## Icinga2 Plugin Check Wordpress

icinga plugin for check wordpress core, plugin, themes update on a remote server.


### How to use

1. Download check_wordpress and save to /lib/nagios/plugins
2. chmod +x check_wordpress
3. Download wp-check.php and save to directory public_html where wordpress located
4. Edit and add your IP
5. Configure your Icinga2

### Icinga

Command Template Icinga2

```
object CheckCommand "check-wordpress" {
    import "plugin-check-command"
    command = [ PluginDir + "/check_wordpress" ]
    arguments = {
        "-H" = "$webaddress$"
        "-S" = {
                set_if = "$usessl$"
        }
        "-P" = "$path$"
    }
}
```

Service Icinga2

```
apply Service "check-wordpress" {
    check_command = "check-wordpress"
    vars.webaddress = "site.com"
    #if use ssl
    vars.usessl = True
    # if remote script has different name
    vars.path = script-name.php
```

Improvment from [here](https://github.com/JMAConsulting/Nagios-WordPress-Update) 
