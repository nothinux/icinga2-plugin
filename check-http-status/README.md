## Check HTTP Status
plugin for icinga2 or nagios for check http status

## How to Use
1. Download check_http_status.sh and save to `/usr/lib64/nagios/plugins`
2. chmod +x check_http_status.sh
3. add check command to `/etc/icinga2/zones.d/global-templates/commands.conf`

   ```
   object CheckCommand "check-http-status" {
       import "plugin-check-command"
       command = [ PluginDir + "/check_http_status.sh" ]
       arguments = {
               "-H" = "$hostname$"
               "-S" = {
                   set_if = "$usessl$"
               }
               "-p" = {
                   set_if = "$port$"
               }
       }
   }
   ```
4. add to Service
   
   ```
   apply Service "nothinux.id status" {
       check_command = "check-http-status"
       vars.hostname = "nothinux.id"
       /* if use ssl *?
       vars.usessl = true
       /* if use diffrent port */
       vars.port = 8080
       assign where host.name == "yourhostname"
   }
   ```

## License
licensed under GPLv2
