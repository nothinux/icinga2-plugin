#!/bin/bash
# writen by Taufik Mulyana, taufik@nothinux.id
# licensed under GPLv2

CURL=`which curl`
USER_AGENT='check-web-availability'
AGENT_OPTS="--user-agent ${USER_AGENT} --insecure"
CURL_OPTS='-Is'
HEAD='head -n1'
SSL=/bin/false

function print_usage
{
        echo "Usage: check-status -H <hostname>"
        echo "  -h, --help  show this help text"
        echo '  -H  <hostname>  Hostname to use.'
        echo "  -S              Use SSL"
        echo "  -p  <port>      if host use different port"
}

STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3

while test -n "$1"; do
    case "$1" in
        --help)
          print_usage
          exit $STATE_OK
          ;;
        -h)
          print_usage
          exit $STATE_OK
          ;;
        -H)
          hostname=$2
          shift
          ;;
        -p)
          port=$3
          shift
          ;;
        -S)
          SSL=/bin/true
          ;;
        *)
          echo "Unknown argument: $1"
          print_usage
          exit $STATE_UNKNOWN
    esac
    shift
done

# set a default for hostname
if [ "$hostname" = "" ]; then
    hostname=$ICINGA_HOSTADDRESS
fi

# set the protocol
if $SSL; then
    protocol="https://"
    port="443"
else
    protocol="http://"
    port="80"
fi

# check http status
response=`$CURL $AGENT_OPTS $CURL_OPTS ${protocol}${hostname}:${port} | $HEAD | cut -d' ' -f2`

if [[ $response == *"200"* ]]; then
    echo "STATUS $response OK"
    exit $STATE_OK
elif [[ $response == *"301"* ]]; then
    echo "STATUS $response Moved Permanently"
    exit $STATE_WARNING
elif [[ $response == *"302"* ]]; then
    echo "STATUS $response Found"
    exit $STATE_WARNING
elif [[ $response == *"400"* ]]; then
    echo "STATUS $response Bad Request"
    exit $STATE_WARNING
elif [[ $response == *"401"* ]]; then
    echo "STATUS $response Unauthorized"
    exit $STATE_WARNING 
elif [[ $response == *"500"* ]]; then
    echo "STATUS $response Internal Server Error"
    exit $STATE_CRITICAL
elif [[ $response == *"501"* ]]; then
    echo "STATUS $response Not Implemented"
    exit $STATE_CRITICAL
elif [[ $response == *"502"* ]]; then
    echo "STATUS $response Bad Gateway"
    exit $STATE_CRITICAL
elif [[ $response == *"503"* ]]; then
    echo "STATUS $response Service Unavailable"
    exit $STATE_CRITICAL
elif [[ $response == *"504"* ]]; then
    echo "STATUS $response Gateway Timeout"
    exit $STATE_CRITICAL
fi
